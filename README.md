A Repo for random Linux Scripts and Configs
===========================================

# What's This? 
This repo should contain more shell scripts and 
random perl or python scripts that do things. 

## Octal Perms  Demonstration
The octal script attempts to show octal permissions in a brute 
force fashion and then show a user what those permissions look
like when running ls -al. The script requires tmux be installed, 
as it opens a tmux session that shows the script running and 
also runs a watch command in order to show the results of 
ls -al. The script should also clean up after itself. 

#### Example: 
[![asciicast](https://asciinema.org/a/OMdnJ3wbValLrmzauhM7d36jt.png)](https://asciinema.org/a/OMdnJ3wbValLrmzauhM7d36jt)

## Gathering Invalid Login Attempts over SSH using fail2ban and log files. 
The invalid_user.sh script is a script that can be run on any 
Linux/Nix system that has fail2ban installed, and a log file 
called /var/log/secure. This script scrapes the /var/log/secure
log file, the current log file not the rotated log files, and then 
pulls uniq ip and username combinations. It attempts to count these 
combinations. It also uses a geoip search to see which country the IP 
originanted from. It's like an elastic search without the rubberband. 
An example report that is created by the script is also contained herein. 

## Ansible Redhat Lab

This project details how to setup an ansible lab using a Fedora laptop and KVM. The goal of the project is to be able to create 3 virts automagically at any time so that the virts can be deleted and recreated at a whim. 


## Kerberos Client and Server Setup Script 
There's a kerberos configuration setup script in this repo. The script uses
sed to modify kadm.acl, /etc/kdc.conf, and the krbkdc conf file. It also adds 
kerberos ports to the firewall using firewallcmd. The script also attempts to install 
firewalld. 

Please note this script expects a user to run as root, IE: sudo. 

## Network Teaming on Linux that uses nmcli. 
I have included a script here that demonstrates in a virtual environment the create of teamed network interfaces. Please see the script itself for detailed comments. 

[![asciicast](https://asciinema.org/a/PNTKfQcJLRYD1OshXi0j99Pim.png)](https://asciinema.org/a/PNTKfQcJLRYD1OshXi0j99Pim)


## jpg mime extrator
I've included a python script to rip jpg from a mime encoded email block. During my testing of PGP with python there was the problem that once decrypted the email was still in mime format. That meant that the JPG data was still encoded yeilding a what is essentially a text file. Using a bit of code borrowed from stack overflow I was able to extract JPG data and turn it into an actual image. This bit of code goes well with my IOT security system, but the code can be used by anyone dealing with MIME. The code can be easily modified to decode any other MIME stuff. 

## Linux Process Watcher
I wanted to create a simple process watcher. The script gets a list of processes at the privilige it was executed at. It will output any new processes it finds, and it will output the PID of any processes that have terminated. Note, if you want to see all processes this script must be run as root. Warning: read and understand the script before running as root (even though the script performs read only operations, it is always a best practice to read any random code before running it...). 

Things to do with this script: 

1. Use psutil process.iterator, and dictionaries. So as to get better informationa but processes that have terminated. 
2. Clean up some of the extra code, and optimize some of the searches. 
3. Learn how to push and pop on a list of dictionaries... 

Here's an asciicast of the process watcher: 


[![asciicast](https://asciinema.org/a/Vun12XbuYkjdEJi6NxQa2dPcC.png)](https://asciinema.org/a/Vun12XbuYkjdEJi6NxQa2dPcC)
