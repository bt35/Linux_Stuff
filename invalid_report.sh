#!/bin/bash 

# This script relies on fail2ban being installed and /var/log/secure 
# It captures the invalid passwords, and ip addresses. Then reports thoses. 
# Then it attempts to GEOIP them. It is a very rudimentary report, but, its cool. 

FROM="user@someplace.com"
TO="user@someplace.com"

# Get invalid passwords
# This could be better. 
sudo grep  ": Invalid user" /var/log/secure | grep -v grep | awk '{ print $8 ":"  $10  }' > ffs_internet_stahp.txt

echo "
----------------------------------
Invalid SSH usernames report:
----------------------------------" > report.txt

# Sort 'em and count 'em. Descending upon occurrence. 
sort ffs_internet_stahp.txt | uniq -c | sort -r -k 1 >> report.txt

# Geoiplookup of the IP and then put that in a file. 
for i in $(sed 's/.*://' ffs_internet_stahp.txt | uniq ); do echo "$i";  geoiplookup $i ; echo ""; sleep 1; done > whois.txt 

echo "
----------------------------------
GEO Location Of All IPs: 
----------------------------------" >> report.txt

cat whois.txt >> report.txt

echo "
----------------------------------
GEO Location Most Popular: 
----------------------------------" >> report.txt

grep GeoIP whois.txt | sed 's/.*://' | sort | uniq -c | sort -r -k 1>> report.txt


mail -r $FROM -s Invalid User Report $TO < report.txt
