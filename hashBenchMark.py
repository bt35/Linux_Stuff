'''
Bench mark hashes
Todo: genreate fake files to use with dev urandom 50 Megabytes. 
'''

from blake3 import blake3
from Crypto.Hash import SHA1, SHA256, SHA512, BLAKE2s, BLAKE2b
import glob
import time
import  sha3


def readFiles():

    fileDir = '/home/locke/hashBenchMarks/filesToHash/*'
    files = glob.glob(fileDir)

    return files

def blake3HashMaker(files):
    hasher = blake3()
    for i in files:
        with open(i, 'rb') as f:
            hasher.update(f.read())
        print (hasher.hexdigest())

def sha3512Maker(files):

    for i in files:
        s = sha3.sha3_512() # also 224, 256, 384, 512
                   # also exposed as the function sha3.sha3_512(...)
        with open(i, 'rb') as f:
            s.update(f.read())
        print(s.hexdigest())

def sha3256Maker(files):

    for i in files:
        s = sha3.sha3_512() # also 224, 256, 384, 512
                   # also exposed as the function sha3.sha3_512(...)
        with open(i, 'rb') as f:
            s.update(f.read())
        print(s.hexdigest())

def sha2512(files):

    for i in files:
        s = SHA512.new() # also 224, 256, 384, 512
                   # also exposed as the function sha3.sha3_512(...)
        with open(i, 'rb') as f:
            s.update(f.read())
        print(s.hexdigest())

def sha2256(files):

    for i in files:
        s = SHA256.new() # also 224, 256, 384, 512
                   # also exposed as the function sha3.sha3_512(...)
        with open(i, 'rb') as f:
            s.update(f.read())
        print(s.hexdigest())

def sha21(files):

    for i in files:
        s = SHA1.new() # also 224, 256, 384, 512
                   # also exposed as the function sha3.sha3_512(...)
        with open(i, 'rb') as f:
            s.update(f.read())
        print(s.hexdigest())

def blake2s(files):

    for i in files:
        s = BLAKE2s.new() # also 224, 256, 384, 512
                   # also exposed as the function sha3.sha3_512(...)
        with open(i, 'rb') as f:
            s.update(f.read())
        print(s.hexdigest())


def blake2b(files):

    for i in files:
        s = BLAKE2b.new() # also 224, 256, 384, 512
                   # also exposed as the function sha3.sha3_512(...)
        with open(i, 'rb') as f:
            s.update(f.read())
        print(s.hexdigest())

def hr(length):
    print('-' * length)


def main():
    print("This did not fail")
    files = readFiles()
    print(files)



    # blake3
    start = time.time()
    blake3HashMaker(files)
    end = time.time() - start
    print("Blake3: duration to hash all files in the list of files: {}".format(end))

    hr(129)

    # sha3_512
    start = time.time()
    sha3512Maker(files)
    end = time.time() - start
    print("sha3_512: duration to hash all files in the list of files: {}".format(end))

    hr(129)


    #sha3 256
    start = time.time()
    sha3256Maker(files)
    end = time.time() - start
    print("sha3_256: duration to hash all files in the list of files: {}".format(end))

    hr(129)


    #sha2 512
    start = time.time()
    sha2512(files)
    end = time.time() - start
    print("sha2_512: duration to hash all files in the list of files: {}".format(end))

    hr(129)


   #sha2 256
    start = time.time()
    sha2256(files)
    end = time.time() - start
    print("sha2_256: duration to hash all files in the list of files: {}".format(end))

    hr(129)


   #sha2 1
    start = time.time()
    sha21(files)
    end = time.time() - start
    print("sha2_1: duration to hash all files in the list of files: {}".format(end))


    hr(129)

  #BLAKE2s
    start = time.time()
    blake2s(files)
    end = time.time() - start
    print("BLAKE2s: duration to hash all files in the list of files: {}".format(end))

    hr(129)


   #BLAKE2b
    start = time.time()
    blake2b(files)
    end = time.time() - start
    print("BLAKE2b: duration to hash all files in the list of files: {}".format(end))

    hr(129)




if __name__=='__main__':
    main()

