A Repo for random Linux Scripts and Configs
===========================================

Finding key words from Reddit post tittles via RSS feeds. 

The graph below shows keywords from the following feed: "https://www.reddit.com/r/aww/top.rss?t=year&limit=50"

![aww](aww.png)

Ditto for r/linux  

![linux](linux.png)

Same for r/crypto

![crypto](crypto.png)
