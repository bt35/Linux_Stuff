import feedparser, string, re, argparse
import pandas as p 
from nltk.probability import FreqDist
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import matplotlib.pyplot as plt
import csv as c

'''
This script is an effort to build a list of key words that I can use to identify likely articles that I would like to read by using NLTK to identify keywords from titles of reddit posts. 

Args: Pass a feed, single feed for now, and identify key words.

Results of the major key words vary... based on timeframe, subreddit, any just about anything really. If something was cool, or big in the news those key words might be more present than others.
'''

split = []
string = ""
filtered = []
sw = stopwords.words("english")

parser = argparse.ArgumentParser()

# Borrowed this code from https://timothybramlett.com/recieving_notifications_of_rss_feeds_matching_keywords_with_Python.html
def readFeeds(feeds):
    for feed in feeds:
        readIt = feedparser.parse(feed)
        for key in readIt["entries"]:
            title = key['title']
            url = key['links'][0]['href']
            split = title.split()
            getKeys(split)

def getKeys(s):
    global string
    for i in s:
        i = re.sub(r'[^\w\s]','',i)
        string = i + " " + string 

# This was a good primer for NLTK: https://www.datacamp.com/community/tutorials/text-analytics-beginners-nltk
def nlStuff(string):

    tokens = []
    tokens = word_tokenize(string)

    # Remove stop words
    
    for i in tokens:
        if i not in sw:
            filtered.append(i)

    fdist = FreqDist(filtered)
    print (fdist)
    fdist.plot(30,cumulative=False)
    plt.show()


def main():
#    parser.add_argument("feed", help="Enter an rss feed.",
#                        type=str)
    parser.add_argument('-f','--feed', action='append', help='Enter an RSS Feed or Feeds.\n\t Put the URLs inside of double quotes. \n\t Otherwise the rss query characters will break the arguments. \n\t Example: -f https://www.reddit.com/r/aww/top.rss?t=year&limit=50')
    args = parser.parse_args()  
    readFeeds(args.feed)
    nlStuff(string)

main()
