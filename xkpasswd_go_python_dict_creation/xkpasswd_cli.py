#!/usr/bin/python3
import string, os, unicodedata, argparse, random, sys
'''
Changing this password generator script into a diciontary creator. 
'''

# get words from dict...
lines = [line.rstrip('\n') for line in open("/usr/share/dict/words")]
select = [] # Make a list of new words to use. The select few without accents and 's.

digits = string.digits
symbols = string.punctuation
rand = random.SystemRandom()

parser = argparse.ArgumentParser(description='Python XKpasswd.net dictionary creator...')
parser.add_argument('-n', help = 'how many passwords to generate? ')
parser.add_argument('-f', help = 'Name of the output file.')

args = parser.parse_args()

# for each word in the lins list, lets select which words to use for a password.
for word in lines:
    if len(word) >= 4 and len(word) <= 8 and "'" not in word:
        no_accents = unicodedata.normalize('NFKD',word).encode('ASCII','ignore')
        select.append(no_accents.decode('utf-8'))

def generate_password(words,symbols,digits):
    # password format:
    # ppddswordsWORDswordsddpp
    #print (symbols)
    p = str(rand.choice(symbols)) # Randomly select a symbol.
    s = str(rand.choice(symbols))
    d1 = str(rand.choice(digits)) + str(rand.choice(digits))
    d2 = str(rand.choice(digits)) + str(rand.choice(digits))
    no_space = ''
    # Compile the password
    password = p + no_space + p + no_space + d1  + no_space + s + no_space \
            + str(rand.choice(words)) + no_space + s + no_space + str(rand.choice(words)).upper() + no_space + s \
            + str(rand.choice(words)) + no_space + s + no_space + d2 + no_space + no_space + p + no_space + p
    return password

if args.n is None and args.f is None:
    parser.print_help(sys.stderr)
    sys.exit(1)
else:
    with open(args.f, 'a') as outFile:
        for i in range(int(args.n)):
            outFile.write(generate_password(select, symbols, digits))
            outFile.write("\n")
