### What's this? 
Once upon a time in a land far, far away a script monkey tried to learn a low level language. So he wrote an implementation of Xkpasswd [1] in **Python** and then wrote the same in **Go**. 

The script monkey was also attempting to learn why High Level languages are a bit slower than low level languages. 

This project will have some results of dictionary creation tests using the unix **time** and **Perf** command. 

In all seriousness though I recommend using the **Perl** Implementation seen in the link below as it is much cleaner and has statistics about the entropy provided by the generated passwords. It also allows for more and different password permutations. 

[1][https://xkpasswd.net/s/](https://xkpasswd.net/s/)

### Python Implementation

The **Python** implementation will create a file and append the file if it is specified in a subsequent execution. Here is an example execution:

```
$ ./xkpasswd_cli.py 
usage: xkpasswd_cli.py [-h] [-n N] [-f F]

Python XKpasswd.net dictionary creator...

optional arguments:
  -h, --help  show this help message and exit
  -n N        how many passwords to generate?
  -f F        Name of the output file.

$ ./xkpasswd_cli.py -n 5 -f test

$ cat test 
++01^Mnidrome^OWENIST^Minne^44++
\\59\digested\BALLADRY\tiriba\80\\
""28;Osman;DUMBED;offramp;34""
((93&Ianthina&LYRID&Bagram&78((
||98:Saul:AFFRONTS:psychism:27||

$ 
```

### Go Implementation

The **Go** implementation will create a file. However if the same file is specified on a subsequent execution it will panic. This needs to be fixed in the future but I am not sure. The **Go** implementation will also create one extra password due to incrementing. I'll fix it later, maybe or maybe not. What is one few bytes... 

```
$ go run xkpasswd-cli.go 
Number of passwords is 0. Please specify how many passwords to generate using:
-num=#'s
outFile: 
Num Passwords: 0

$ go run xkpasswd-cli.go -num=5 
Output file was not specified. Please specify using:
-outFile=nameoffile
outFile: 
Num Passwords: 5

$ go run xkpasswd-cli.go -num=5 -outFile=test

$ cat test 
::11\Bolshevist\SQUALLING\linnaean11::
__44{quasiirregularly{METOPES{outerly44__
``11,grigri,PREHAPS,nontolerance11``
&&88)dockyards)FISHHOOK)wadmels88&&
$$66?prankishness?SHANLY?toop66$$
||44$ladronism$HEATTREAT$roseheaded44||

$ 

```

### Test Results 

I ran the following two tests to see which implementation is faster. I totally expect that the one in **Go** would be faster, and can be made much faster using go routines. 

```
# Python Test:
or i in {1..10}; do e=$(($i**5)); echo "Generating $e passwords."; time ./xkpasswd_cli.py -n $e -f test-$e; done &> results_py.test

# Go Test
for i in {1..10}; do e=$(($i**5)); echo "Generating $e passwords."; time go run xkpasswd-cli.go -num=$e -outFile=test-$e; done &> results_go.test
```

Here is the output of the two results_*.test files. 

These tests were run on an **Brand: Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz**. 

**Python** Results| Time |**Go** Results| Time|
:-----:|:-----:|:-----:|:-----:
Generating 1 passwords| |Generating 1 passwords| 
real|    0m1.053s|real|0m1.222s
user|   0m1.042s|user|0m1.351s
sys|     0m0.012s|sys|0m0.170s
Generating 32 passwords| |Generating 32 passwords| 
real|    0m0.987s|real|0m1.249s
user|    0m0.963s|user|0m1.395s
sys|    0m0.021s|sys|0m0.158s
Generating 243 passwords| |Generating 243 passwords| 
real|    0m1.016s|real|0m0.987s
user|   0m0.963s|user|0m1.138s
sys|     0m0.051s|sys|0m0.103s
Generating 1024 passwords| |Generating 1024 passwords| 
real|   0m1.219s|real|0m0.982s
user|    0m1.178s|user|0m1.117s
sys|     0m0.041s|sys|0m0.121s
Generating 3125 passwords| |Generating 3125 passwords| 
real|    0m1.733s|real|0m1.001s
user|    0m1.611s|user|0m1.103s
sys|     0m0.121s|sys|0m0.103s
Generating 7776 passwords| |Generating 7776 passwords| 
real|    0m2.861s|real|0m0.969s
user|    0m2.450s|user|0m1.103s
sys|    0m0.411s|sys|0m0.110s
Generating 16807 passwords| |Generating 16807 passwords| 
real|    0m4.934s|real|0m1.018s
user|    0m4.443s|user|0m1.140s
sys|     0m0.490s|sys|0m0.149s
Generating 32768 passwords| |Generating 32768 passwords| 
real|    0m9.357s|real|0m1.173s
user|    0m8.188s|user|0m1.264s
sys|    0m1.160s|sys|0m0.191s
Generating 59049 passwords| |Generating 59049 passwords| 
real|   0m15.880s|real|0m1.234s
user|    0m14.216s|user|0m1.272s
sys|     0m1.660s|sys|0m0.247s
Generating 100000 passwords| |Generating 100000 passwords| 
real|    0m26.550s|real|0m1.254s
user|    0m23.700s|user|0m1.216s
sys|    0m2.790s|sys|0m0.286s

### Testing a Huge Dict File. 

Lets create a dict file of 1000000 unique xkpasswds. The go process finishes much faster, so much so that I am not sure we can trust the output. 

**Python One**

```
$ perf stat ./xkpasswd_cli.py -n 1000000 -f test

 Performance counter stats for './xkpasswd_cli.py -n 1000000 -f test':

         52,438.10 msec task-clock:u              #    1.000 CPUs utilized          
                 0      context-switches:u        #    0.000 K/sec                  
                 0      cpu-migrations:u          #    0.000 K/sec                  
            13,233      page-faults:u             #    0.252 K/sec                  
    66,485,958,512      cycles:u                  #    1.268 GHz                    
   157,056,815,933      instructions:u            #    2.36  insn per cycle         
    34,849,790,758      branches:u                #  664.589 M/sec                  
       137,860,318      branch-misses:u           #    0.40% of all branches        

      52.464186017 seconds time elapsed

      38.466331000 seconds user
      13.366139000 seconds sys

# Are there any duplicates? No. However please see the warning in the next Go section about duplicate symbol combinations or strings appearing in the same position again. 

$ uniq -d test
$ wc -l test
1000000 test
$ head test
&&49/trifler/DELIMES/detin/02&&
&&92_pacifier_RERACKER_barrater_34&&
]]03[emboss[EXCLUDES[nerine[83]]
[[28]Ledidae]MILLIE]Nations]58[[
++54(boeotian(RIPENS(quirkish(22++
))82+genista+SPANING+ental+17))
``24'Kutuzov'TIE-WIG'gabbed'77``
@@46]swervers]DIAPERS]kakariki]99@@
##65,byerlite,GAUSTER,OLIT,97##
~~58~behang~LAPILLUS~GATV~73~~
$ 
```

**Go One**

```
$ perf stat go run xkpasswd-cli.go -num=1000000 -outFile test.go

 Performance counter stats for 'go run xkpasswd-cli.go -num=1000000 -outFile test.go':

          6,197.91 msec task-clock:u              #    1.079 CPUs utilized          
                 0      context-switches:u        #    0.000 K/sec                  
                 0      cpu-migrations:u          #    0.000 K/sec                  
            35,042      page-faults:u             #    0.006 M/sec                  
     6,069,128,272      cycles:u                  #    0.979 GHz                    
    10,545,535,957      instructions:u            #    1.74  insn per cycle         
     2,187,150,155      branches:u                #  352.885 M/sec                  
        11,187,610      branch-misses:u           #    0.51% of all branches        

       5.743550115 seconds time elapsed

       3.903135000 seconds user
       2.281813000 seconds sys
$

# Are there any duplicate strings in that big file? 

$ uniq -d test.go 
$ wc -l test.go 
10000001 test.go
$ head test.go 
{{77|blowups|FALCIFORM|Lyford77{{
__55=xanthometer=FIREDRAGON=tymbalon55__
}}77`excitant`CONFIT`rachidial77}}
//11|presymptom|NEWGROUND|periarthric11//
**88{garrisoning{INEFFERVESCIBLE{husbandfield88**
^^11}levin}AMICROBIC}twosoused11^^
<<22<cormous<UNCOMFORTABLY<subpastorship22<<
~~99_McDougall_RESILES_carone99~~
::22-premunire-RESERVATIONS-uphroes22::
@@22;nonconsenting;PARALLELOGRAMMICAL;calcarium22@@
$ 
```

##### Running the Same Tests above on a Raspberry Pi 3 B+

**Python One**

```
$ perf stat ./xkpasswd_cli.py -n 1000000 -f test

 Performance counter stats for './xkpasswd_cli.py -n 1000000 -f test':

        254,530.81 msec task-clock:u              #    0.997 CPUs utilized          
                 0      context-switches:u        #    0.000 K/sec                  
                 0      cpu-migrations:u          #    0.000 K/sec                  
             2,587      page-faults:u             #   10.164 M/sec                  
   285,988,215,822      cycles:u                  # 1123593.352 GHz                 
   173,517,936,063      instructions:u            #    0.61  insn per cycle         
    11,092,319,546      branches:u                # 43579615.550 M/sec              
     2,545,568,577      branch-misses:u           #   22.95% of all branches        

     255.239565160 seconds time elapsed

     229.224281000 seconds user
      25.300472000 seconds sys
$ 
$ wc -l test
1000000 test
$ uniq -d test
$ 

```

**Go One**

```
$ perf stat go run xkpasswd-cli.go -num=1000000 -outFile=test.go

 Performance counter stats for 'go run xkpasswd-cli.go -num=1000000 -outFile=test.go':

         23,309.50 msec task-clock:u              #    0.989 CPUs utilized          
                 0      context-switches:u        #    0.000 K/sec                  
                 0      cpu-migrations:u          #    0.000 K/sec                  
            16,387      page-faults:u             #  703.033 M/sec                  
    18,387,031,079      cycles:u                  # 788838.263 GHz                  
    13,074,073,264      instructions:u            #    0.71  insn per cycle         
     1,388,164,447      branches:u                # 59554869.235 M/sec              
       139,835,577      branch-misses:u           #   10.07% of all branches        

      23.557539546 seconds time elapsed

      13.450990000 seconds user
       9.862450000 seconds sys
$
$ wc -l test.go 
1000001 test.go
$ uniq -d test.go 
```

#### A Note About Duplicate Symbols

There might be duplicate symbol combinations and perhaps words used in the same position, but no password should be an exact match of another. For example see the password below. See how the first and last set of symbols matches the delimiter symbol, that is sheer random chance, since each set of symbols is give its own random choice. However this will happen since both symbol variables have the same character set, and it is a limited character set. 

```
<<22<cormous<UNCOMFORTABLY<subpastorship22<<
```

Duplication generally makes a password less secure however the search space of the entire password above is massive so it is theoretically impossible to break by brute force. [*] Most if not all passwords that conform to some sort of standard theoretically may be vulnerable to dictionary attacks. The processing cost is still very high, either during dictionary creation or during the hash comparison. 

[*] Crack-lib shows the password seen above is ok! Note, those password testing sites estimate that the password below might take a few Trillion Trillion years. [2]

```
$ echo "<<22<cormous<UNCOMFORTABLY<subpastorship22<<" | cracklib-check 
<<22<cormous<UNCOMFORTABLY<subpastorship22<<: OK
```

[2 Rumkin Check](http://rumkin.com/tools/password/passchk.php)
