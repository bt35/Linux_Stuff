package main

import (
	"bufio" // buffer for reading files.
	"bytes"
	"flag" // parse some args.
	"fmt"  // import format lib
	"math/rand"
	"os"     // Open files.
	"regexp" // need this to get rid of words that have - and ` and '
	"strings"
	"time"
)

// Declarations
// Check for errors when reading files and buffer scanning.
func check(e error) {
	if e != nil {
		panic(e) // PANIC! HACF
	}
}

// Read /usr/share/dict/american-english and
// make words...
// Need to ignore words with apostrophies and dashes.
// Forgive me.. if I still do things the python way... this
// low level stuff is difficult.
func getWords() []string {

	// Tell Go to open a dictionary.
	// May need a command line arg for this... Not every distro has this dict.
	//dat, err := os.Open("/usr/share/dict/american-english")
	dat, err := os.Open("/usr/share/dict/words")
	check(err)        // Check for read errors.
	defer dat.Close() // CLOSE THE FILE LATER, hopefully.

	// I think this is for scanning lines from the file.
	scanner := bufio.NewScanner(dat)
	scanner.Split(bufio.ScanWords)

	// Regex to remove lines with puncuation.
	reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	check(err) // Again check errors.

	var words []string // Array of words used to store all the lines of the dict file.

	// move the scan buffer items to the words array.
	for scanner.Scan() {
		words = append(words, scanner.Text())
	}

	// place holder variable for each word.
	var newWord string
	newWord = ""
	var noPunct []string // Array to store all the words that do not have punctuation.

	/* Read the words array, ignore words with punctiation
	   then add the word to the newWord placeholder.
	   then move the newWord to the new array.
	   I think this can be more efficient.  */

	for _, word := range words {
		newWord = reg.ReplaceAllString(word, "")
		noPunct = append(noPunct, newWord)
	}
	// Return the array of words that does not have punctuation.
	return noPunct
}

// Stub for generating passes.
// This is where we will create a string that looks like this:
// ssddsswordsWORDswordspp
/*
   p = str(rand.choice(symbols)) # Randomly select a symbol.
   s = str(rand.choice(symbols))
   d1 = str(rand.choice(digits)) + str(rand.choice(digits))
   d2 = str(rand.choice(digits)) + str(rand.choice(digits))


*/
func genPass(count int, words []string, out string) {
	// Vars, that only need scope in this func.
	var digit string
	var symbol string
	//var password string
	var symbol2 string
	var sBuff bytes.Buffer
	// Init rng
	rand.Seed(time.Now().Unix())
	digits := [9]string{"1", "2", "3", "4", "5", "6", "7", "8", "9"}
	symbols := [33]string{"!", "\"", ".", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]", "^", "_", "`", "{", "|", "}", "~"}
	oFile, err := os.OpenFile(out, os.O_APPEND|os.O_CREATE|os.O_EXCL|os.O_WRONLY, 0666)
	check(err)          // Check for read errors.
	defer oFile.Close() // CLOSE THE FILE LATER, hopefully.
	for count >= 0 {
		fmt.Printf("\rProgress: %d", count)
		fmt.Println()
		digit = digits[rand.Intn(len(digits))]
		symbol = symbols[rand.Intn(len(symbols))]
		symbol2 = symbols[rand.Intn(len(symbols))]
		// Clean the buffer.
		sBuff.Reset()
		sBuff.WriteString(symbol)
		sBuff.WriteString(symbol)
		sBuff.WriteString(digit)
		sBuff.WriteString(digit)
		sBuff.WriteString(symbol2)
		sBuff.WriteString(words[rand.Intn(len(words))])
		sBuff.WriteString(symbol2)
		sBuff.WriteString(strings.ToUpper(words[rand.Intn(len(words))])) // Need to make upper case.
		sBuff.WriteString(symbol2)
		sBuff.WriteString(words[rand.Intn(len(words))])
		sBuff.WriteString(digit)
		sBuff.WriteString(digit)
		sBuff.WriteString(symbol)
		sBuff.WriteString(symbol)
		sBuff.WriteString("\n")
		//fmt.Println(sBuff.String())
		if _, err = oFile.WriteString(sBuff.String()); err != nil {
			panic(err)
		}
		// Better decrement...
		count = count - 1
		// Need to append the passwords to a file.

	}

	// Debugging stuff. Print out each little thing we are testing.
	/*
		fmt.Println(count)
		fmt.Println(digits)
		fmt.Println(symbols)
		fmt.Println(digit)
		fmt.Println(symbol)
		fmt.Println(words[rand.Intn(len(words))])
	*/
}

// Main Function.
// Testing args...
func main() {
	outFilePtr := flag.String("outFile", "", "Output File Name.")

	// default to 0...
	// need to implement argument checking.
	numPassPtr := flag.Int("num", 0, "Number of Passwords to Generate.")

	flag.Parse()
	if *numPassPtr == 0 {
		fmt.Println("Number of passwords is 0. Please specify how many passwords to generate using:")
		fmt.Println("-num=#'s")
		fmt.Println("outFile:", *outFilePtr)
		fmt.Println("Num Passwords:", *numPassPtr)
	} else if len(*outFilePtr) <= 1 {
		fmt.Println("Output file was not specified. Please specify using:")
		fmt.Println("-outFile=nameoffile")
		fmt.Println("outFile:", *outFilePtr)
		fmt.Println("Num Passwords:", *numPassPtr)
	} else {
		genPass(*numPassPtr, getWords(), *outFilePtr)
	}

}
