#!/bin/bash 

# This script is built to create an active backup NIC team for Rhel7. 
# This script was built with a Centos 7 Virt using KVM. The Teamed NICs 
# added via the VM options. Those NICs are mapped to an empty bridge on 
# the VM host called br0. 

# To create an empty bridge on the host server follow this guide: 
# https://www.cyberciti.biz/faq/how-to-add-network-bridge-with-nmcli-networkmanager-on-linux/. 
# To ensure that the bridge is empty ensure that no other NIC on the server has an attribute 
# BRIDGE="br0" within its configuration file. 
# I created a bridge without nmcli by simply making a ifcfg-br0. 
# DEVICE=br0
# TYPE=Bridge
# IPADDR=172.16.0.1
# NETMASK=255.255.255.0
# ONBOOT=yes
# BOOTPROTO=none
# NM_CONTROLLED=no
# DELAY=0
#

# For the purposes of Virts I prefer to use ifconfig, as I am still working my way into using nmcli. 

# Let's create a Team. 
# My virtual server has the following interfaces: 
# ens9, and ens10. These are the interfaces that are part of the empty bridge. 
# ens9, and ens10 will make up a team.  

# Flip down up function
function flip_interfaces(){
	nmcli con down bridge-team &> /dev/null
	nmcli con down bridge-team-port1 &> /dev/null
	nmcli con down bridge-team-port2 &> /dev/null

	nmcli con up bridge-team	
	nmcli con up bridge-team-port1
	nmcli con up bridge-team-port2
}

# Let's create an active backup team. 
nmcli con add type team con-name bridge-team ifname bridge-team config '{"runner": {"name":"activebackup"}}'
# add an ip
nmcli con mod bridge-team ipv4.addresses 172.16.0.100/24 
# set the ip static, no dhcp. 
nmcli con mod bridge-team ipv4.method manual 
# add slaves to the team. 
nmcli con add type team-slave con-name bridge-team-port1 ifname ens9 master bridge-team
nmcli con add type team-slave con-name bridge-team-port2 ifname ens10 master bridge-team 

# Flip down and up the interfaces starting with the master. 
# show the configuraton. 
teamdctl bridge-team state

# Give some to to view the changes. 
sleep 5


# create the other types of teams:  
nmcli con mod bridge-team config '{"runner": {"name":"roundrobin"}}'
sleep 1
flip_interfaces
# Show new config
teamdctl bridge-team state
sleep 5

nmcli con mod bridge-team config '{"runner": {"name":"broadcast"}}'
sleep 1
flip_interfaces
# Show new config
teamdctl bridge-team state
sleep 5

# clean up 
nmcli con del bridge-team &> /dev/null
nmcli con del bridge-team-port1 &> /dev/null
nmcli con del bridge-team-port2 &> /dev/null


