#!/bin/bash
echo "Using OpenSSL to encrypt a bunch of files with all available algorithms."
echo "Then use ImageMagick to convert the encrypted files to grayscale representations of the data."
echo "We'll also convert the un-encrypted files to grayscale representation of the data."
echo "Then ... compare the output!"
echo "Maybe we can see differences in each algorithm, or only differences in the *weaker* algorithms."

# get cipherfile
openssl enc -list | awk '{print $NF}' | sed -e '1d' > ciphers.txt

declare -a CIPHERS
# Read open ssl cipher list... for the -cipher_command
readarray CIPHERS < ciphers.txt

PASS=$(openssl rand -base64 21)
echo -e "Using key $PASS"
TEXT=$(curl metaphorpsum.com/paragraphs/20)

echo $TEXT | wc -c

# Create nonsense files for each cipher.

## Clean up from the last run.
echo "deleting old stuff"
rm ./files/*
rm ./data/*
rm ./png/*

## These for loops can be added to one big loop. Create text file, encrypt it, and then convert to a PNG.
## I like them separate, since I find bash incredibly difficult to read.
echo -e "Creating files for each of the OpenSSL Cipher Algo's available. "
printf "%0.s$TEXT" {1..209} > files/plain


## encrypt the files. Some of the algorithms may fail... it happens.
## Error checking to be added later? Maybe...
echo -e "Encrypting the files..."
# for i in number_ciphers
for i in "${CIPHERS[@]}"
do
    :
    echo -e "Encrypting sample data with: $i \n"
    openssl enc $i -in files/plain -out data/data.$i -pass pass:$PASS -pbkdf2 -iter 100000
done

ls -l data

sleep 2

## Error checking to be added later? Maybe...
echo "Making Picutres..."
# for i in encrypted files
for i in $(ls data/)
do
    :
    echo $i
    #cat data/$i | convert -size 1024x1024 -depth 8 gray:- png/gray-$i.png
    #cat data/$i | convert -size 1024x1024 -crop 100x100+0+0 -scale 300x300 png/mono-$i.png
    convert -size 2896x2896 "mono:data/$i" -crop 100x100+0+0 -scale 300x300 "png/mono-$i.png"
    convert -size 1024x1024 -depth 8 "gray:data/$i" -scale 300x300 "png/gray-$i.png"
done

#for i in $(ls files/)
#do
#    :
#    convert -size 2896x2896 mono:files/$i -crop 100x100+0+0 -scale 300x300 png/mono-$i.png
#    convert -size 1024x1024 -depth 8 gray:files/$i -scale 300x300 png/gray-$i.png
#done

# End?
