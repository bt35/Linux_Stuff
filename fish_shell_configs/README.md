# What's this? 
This little directory should contain interesting stuff for fish shell.

## Prompt:
My bash prompt has emoticons at the front that change depending on the status of
the last run command. An excellent guide on shell configuration can be found
here: https://www.booleanworld.com/customizing-coloring-bash-prompt/. 

```
^_^ [someone@someplaces:pwd:]

X_X [someone@someplaces:pwd:]
```


Here's the PS1 for the bash prompt: 
```
  [ "$PS1" = "\\s-\\v\\\$ " ] &&  PS1="\`if [ \$? = 0 ]; then echo \[\e[32m\]^_^\[\e[0m\]; else echo \[\e[31m\]X_X\[\e[0m\]; fi\`$BWhite[$Blue\u$Red@$Cyan\h:$Green\w$BWhite]$Color_Off\\$ " 
```

Now for the fish! Getting the emoticons in your fish prompt is fairly easy.
Using fish's ```fish_config``` you can select which prompts you would like from
a list of examples. One of the examples is a status prompt, that shows the
status of the last command. Using that same function we can swap in an emoticon
for the status, and add an else statement for the other emoticon. 

```
function fish_prompt --description 'Write out the prompt'
	# Save our status
    set -l last_status $status

    set -l last_status_string ""
    if [ $last_status -ne 0 ]
        printf "%s(%s)%s " (set_color red --bold) X_X (set_color normal)
	else #if [ $last_status -ne 1 ]
		printf "%s(%s)%s " (set_color green --bold) \^\_\^ (set_color normal)
    end

    set -l color_cwd
    set -l suffix
    switch "$USER"
        case root toor
            if set -q fish_color_cwd_root
                set color_cwd $fish_color_cwd_root
            else
                set color_cwd $fish_color_cwd
            end
            set suffix '#'
        case '*'
            set color_cwd $fish_color_cwd
            set suffix '>'
    end

    echo -n -s "$USER" @ (prompt_hostname) ' ' (set_color $color_cwd) (prompt_pwd) (set_color normal) "$suffix "
end

```

To add the prompt without modifying and or creating your own prompt function do
the following: 

1. mkdir ~/.config/fish/functions/
2. touch ~/.config/fish/functions/fish_prompt.fish
3. Paste the contents of the fish function above into the file we touched in
   step 2. 
   
   
