#!/usr/bin/python3

import sys, os, psutil, argparse, time


# Instantiate some stuff...

# Get some arguments... 

# Get a list of running processes: 

def get_procs():
    running = []
    pids = psutil.pids()
    #print (pids)
    for i in pids:
        p = psutil.Process(i)
        print ("[+] Process Found:\n\t Executable: {} \n\t Parent Process: {}".format(p.exe(), p.parent()))
        running.append(p)
    return (running)

def main():
 
    initial_processes = get_procs() # Get first list of processes. 
    print (initial_processes)
main()
