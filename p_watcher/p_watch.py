#!/usr/bin/python3

import sys, os, psutil, argparse, time

parser = argparse.ArgumentParser()
parser.add_argument("-i", help="The interval of which this process watcher should wait before checking for new processes. The interval can be in seconds. IE: .02. 5. 500")
args = parser.parse_args()

interval = args.i 

# Get a list of running processes. This function is not needed anymore.
def get_procs(pids):
    running = []
    for i in pids:
        p = psutil.Process(i)
        #print ("[+] Process Found:\n\t Executable: {} \n\t Parent Process: {}".format(p.exe(), p.parent()))
        running.append(p)
    return (running)

def check_for_new_processes(pids):
    set(pids)                    # Make a set out of the initial processes passed into this function. 
    new_check = psutil.pids()    # Check for new processes. 
    difference = [i for i in new_check if i not in pids]    # I think this is list comprehension. Borrowed from stack overflow. 
                                                            # It is a nicer way of checking if something in one list is not in another list. 
    for i in difference:                                    # loop through the differences. Then print information about the different processes. 
        p = psutil.Process(i)
        print ("[+] New Process Found:\n\t Process Name: {}\n\t Process ID: {} \n\t Executable: {} \n\t Parent Process: {}".format(p.name(),i,p.exe(), p.parent()))
    return (difference)                                     # Return a variable for debugging. 


# Check if old processes are there... push and pop list, I guess. 
def check_old_processes(new_list, old_list):
    set(new_list)
    set(old_list)    
    old_gone   = [i for i in old_list if i not in new_list]
    for i in old_gone:
        print ("[-] Process Gone: {} ".format(i))
   # print (old_gone)
    return (old_gone)


# There is a chance that a process stops existing and that might cause an exception. I'm not sure what exception to catch yet, so ... the script my segfault. 

def main():
    hold_list = []
    changed_list = []
    while (True):
        initial_processes = psutil.pids()                 # Check the processes at the start of the loop. 
        for proc in psutil.process_iter():
            try:
                 pinfo = proc.as_dict(attrs=['pid', 'name', 'exe'])
            except psutil.NoSuchProcess:
                pass
            else:
                hold_list.append(pinfo)

        time.sleep(float(interval))                       # Busy wait. This interval should be an argument. 
        diff = check_for_new_processes(initial_processes) # Check the processes by using the initial process
                                                          # check against another process check in function at line number 22. 
        #check_old_processes(initial_processes, hold_list)
        diff_list = [x for x in initial_processes if x not in psutil.pids()]
        
        for i in diff_list:
        #    if not any(d.get('pid', None) == i for d in hold_list):
            print("[-] Process with ID: {}, is gone.".format(i))
        
        #print(len(initial_processes))                    # This print statement checks to see if initial_processes is actually not growing in size. 

main()
