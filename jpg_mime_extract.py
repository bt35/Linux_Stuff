#!/usr/bin/python3

###
# This is a tool created to extract attachments from a mime 
# message. 
# Thanks to the helpful people here: 
# https://stackoverflow.com/questions/4067937/getting-mail-attachment-to-python-file-object
###

# Import Stuff. Email for handling mime. 
# Getopt for handling file options
# Sys for handling sysv args. 
import email, getopt, sys

# Put the stack overflow stuff into a function and change the param to handle a
# file name. The file name is set via the getopt stuff in the main() method. 
def getAttach(filename):

    # variable for message/ 
    msg = email.message_from_file(open(filename))

    # This tool is built to get the first item out of the mime message. 
    attachment = msg.get_payload()[0]
    open('attachment', 'wb').write(attachment.get_payload(decode=True))

def usage():
    print ('----------------------------------------------------------------')
    print ('This Tool is for getting an encoded message out of a mime file.')
    print ('It pulls the mime encoded message from index 0.')
    print ('It does not expect any other attachments.')
    print ('Specify a mime email message after the -f argument.')
    print ('----------------------------------------------------------------')

# Main method. Handle arguments. 
def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:v", ["help", "filename="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    filename = None
    verbose = False
    if len(sys.argv) == 1:
        print("Please specify a file name.")
        usage()
        sys.exit()
    for o, a in opts:
        if o == "-v":
            verbose = True
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-f", "--filename"): # Get the file name. 
            getAttach(a)                # Run the decode image code. 
        else:
            assert False, "unhandled option"

if __name__ == "__main__":
    main()
