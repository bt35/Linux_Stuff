#!/bin/bash 

# This is a bash script for modifying KDC configurations. 
# I created this script as a way to cement my study of Kerberos
# configurations for the RHCE. The goal of the script is to...
# remove the need for using vim to edit a bunch of files, every, single, 
# time, I want to study kerberos setup. 

# Variables for domain, lowercase domain, and the KDC server. 
DOMAIN="SOMENONSENSE.ORG"
lower_case="somenonsense.org"
SERVER="test2.somenonsense.org"


# change domains and stuff for KDC5 config, and kadmin acl. 
sed -i "s/example.com/$lower_case/" /var/kerberos/krb5kdc/kdc.conf
sed -i "s/EXAMPLE.COM/$DOMAIN/" /var/kerberos/krb5kdc/kdc.conf
sed -i "s/EXAMPLE.COM/$DOMAIN/" /var/kerberos/krb5kdc/kadm5.acl

# Modify the /etc/krb5.conf
# fix default realm. 
sed -i "s/\# default_realm = EXAMPLE.COM/ default_realm \= $DOMAIN/" /etc/krb5.conf
sed -i "s/\# \}/ \}/" /etc/krb5.conf

#Fix realms config. 
sed -i "s/\# EXAMPLE.COM/$DOMAIN/" /etc/krb5.conf
sed -i "s/\#  kdc \= kerberos.example.com/  kdc \= $SERVER/" /etc/krb5.conf
sed -i "s/\#  admin_server \= kerberos.example.com/  admin_server \= $SERVER/" /etc/krb5.conf

sed -i "s/\# .example.com \= EXAMPLE.COM/ .$lower_case \= $DOMAIN/"   /etc/krb5.conf
sed -i "s/\# example.com \= EXAMPLE.COM/ $lower_case \= $DOMAIN/"   /etc/krb5.conf

# Create the kerberos database: 
kdb5_util create -s -r $DOMAIN

# enable and turn on 
systemctl enable krb5kdc kadmin
systemctl start krb5kdc kadmin

# install firewalld. 
yum install firewalld
systemctl enable firewalld
systemctl start firewalld

# For the life of me, I cannot figure out why XML service files do work. 
# It is probably due to me not sufficiently reading TFM. 
firewall-cmd --permanent --add-port=88/tcp 
firewall-cmd --permanent --add-port=88/udp
firewall-cmd --permanent --add-port=749/tcp 
