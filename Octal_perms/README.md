Bash Tmux Script to Show Octal Permissions
==========================================

Requirements
------------

* Tmux 
* Bash
* Non Root User... please. 

What does the script do?
------------------------

The script called tmux_perms_demo.sh attempts to create two panes in a new tmux session. In the bottom pane a watch command will be created to list the file contents of a directory called perms_demo, and all of its contents. The top pane will then execute the perms_test.sh script which attempts to create the directory called perms_demo, and then create 6 empty files with which it will then attempt to change file permissions on. 

The goal of this script is to simply show what octal permissions look like when a directory is listed line by line, IE: *LS -al* 


Here's a gif:

![A GIF](/Octal_perms/new_octal.gif)
