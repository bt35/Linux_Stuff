#!/bin/bash 

echo "Attempt to show all possible permissions combinations on a set of files"
echo "Make a directory and files:"
mkdir perms_demo
for i in {0..5}; do echo "Making new_file$i.empty"; touch perms_demo/new_file$i.empty; sleep .5; done 

#echo "Setting an file access list to allow the user to see the directory even when octal perms change."
#setfacl -R -m u:$USER:rwx ~/perms_demo
#echo "Show the new FACL:"
#getfacl ~/perms_demo
#sleep 2

echo "Showing Special Permssions:"
for i in 0000 1000 2000 3000 4000 5000 6000 7000; do echo "chmod $i perms_demo/*"; chmod $i perms_demo/*; sleep 1; done
sleep 1

echo "Showing octal permissions "
for a in {0..7}
do 
	A=$a
	for b in {0..7}
	do 
		B=$b
		for c in {0..7}
		do 
			C=$c
echo "Running chmod $A$B$C"
chmod  $A$B$C perms_demo/*
sleep 1
done
done
done


echo "Cleaning up:"
sleep 1
chmod  700 perms_demo/*
rm -rf perms_demo

echo "Done."
sleep 1
exit	
