#!/bin/bash 

SESSION_NAME="OCTAL_DEMO"

tmux has-session -t ${SESSION_NAME}

if [ $? != 0 ]
then 
	tmux new-session -d -n OCTAL_DEMO -s ${SESSION_NAME}
	tmux new-window -n octal_demo -t ${SESSION_NAME}

	tmux split-window -v -t 0
	tmux split-window -v -t 1

	tmux send-keys -t 1 'exec   ./perms_test.sh' C-m
	tmux send-keys -t 2 'watch -n 1 "ls -al perms_demo/"' C-m

	tmux select-window -t ${SESSION_NAME}:0
fi 

asciinema rec -c "tmux attach -t ${SESSION_NAME}"
#tmux attach -t ${SESSION_NAME}
