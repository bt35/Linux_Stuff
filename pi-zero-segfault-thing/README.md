### What is this? 

This is a problem I am working on for my Home Assistant Project (Hassio). At somepoint after Hassio was installed and running the WIFI stopped working. When I was troubleshooting that issue I found that **dhcpcd** was constantly trying to clean stale PIDs related to /sbin/resolvconf. Eventually **dhcpcd** would timeout and fail to bring up network interfaces. I thought that manually removing the stale **PID** files and lock files would make it so **dhcpcd** could start. That was when I found that **/bin/rm** was the cause of all of this! When I tried to remove any files **rm** would **segfault**!.

Here is a snippert of **journalctl -u dhcpcd** as it was trying to kill stale PIDs. Output below is chopped due to using the UART Serial connection and minicom. 

```
Apr 17 02:08:44 raspberrypi dhcpcd[204]: clearing stale lock pid 380
Apr 17 02:08:44 raspberrypi dhcpcd[204]: Segmentation fault
Apr 17 02:08:44 raspberrypi dhcpcd[204]: /sbin/resolvconf: 733: kill: No such pr
Apr 17 02:08:44 raspberrypi dhcpcd[204]: clearing stale lock pid 380
Apr 17 02:08:44 raspberrypi dhcpcd[204]: Segmentation fault
Apr 17 02:08:44 raspberrypi dhcpcd[204]: /sbin/resolvconf: 733: kill: No such pr
Apr 17 02:08:44 raspberrypi dhcpcd[204]: clearing stale lock pid 380
Apr 17 02:08:44 raspberrypi dhcpcd[204]: Segmentation fault
Apr 17 02:08:44 raspberrypi dhcpcd[204]: /sbin/resolvconf: 733: kill: No such pr
Apr 17 02:08:44 raspberrypi dhcpcd[204]: clearing stale lock pid 380
Apr 17 02:08:44 raspberrypi dhcpcd[204]: Segmentation fault
Apr 17 02:08:44 raspberrypi dhcpcd[204]: /sbin/resolvconf: 733: kill: No such pr
Apr 17 02:08:44 raspberrypi dhcpcd[204]: clearing stale lock pid 380
```

The wifi issue might might be due to a power issue. The Pi W Zero requires 5v 2.5 Amps. This carrier drops did not go away after I changed out the power supply. Therefore I might have borked the board but I don't know how to test for exlcuding that. 

```
Apr 17 02:07:26 raspberrypi dhcpcd[204]: wlan0: waiting for carrier
Apr 17 02:07:26 raspberrypi dhcpcd[204]: wlan0: carrier acquired
Apr 17 02:07:26 raspberrypi dhcpcd[204]: DUID 
Apr 17 02:07:26 raspberrypi dhcpcd[204]: wlan0: IAID 
Apr 17 02:07:26 raspberrypi dhcpcd[204]: wlan0: adding address 
Apr 17 02:07:26 raspberrypi dhcpcd[204]: wlan0: carrier lost
Apr 17 02:07:26 raspberrypi dhcpcd[204]: Segmentation fault
Apr 17 02:07:26 raspberrypi dhcpcd[204]: Segmentation fault
Apr 17 02:07:26 raspberrypi dhcpcd[204]: wlan0: deleting address 
Apr 17 02:07:26 raspberrypi dhcpcd[204]: wlan0: carrier acquired
Apr 17 02:07:26 raspberrypi dhcpcd[204]: wlan0: IAID 
Apr 17 02:07:26 raspberrypi dhcpcd[204]: wlan0: adding address 
Apr 17 02:07:26 raspberrypi dhcpcd[204]: wlan0: carrier lost
Apr 17 02:07:26 raspberrypi dhcpcd[204]: /sbin/resolvconf: 733: k 
```

#### Platform Details: 

```
PRETTY_NAME="Raspbian GNU/Linux 9 (stretch)"
NAME="Raspbian GNU/Linux"
VERSION_ID="9"
VERSION="9 (stretch)"
ID=raspbian
ID_LIKE=debian
HOME_URL="http://www.raspbian.org/"
SUPPORT_URL="http://www.raspbian.org/RaspbianForums"
BUG_REPORT_URL="http://www.raspbian.org/RaspbianBugs"
```


### Basic GDB output with no symbols. 

Below is the GDB output before I compiled an **Arm** 32 bit version of /bin/rm that was not stripped of debug symbols. (Thanks to GNU devs for making compilation so easy!). 

```
Reading symbols from /bin/rm...(no debugging symbols found)...done.
(gdb) run test
Starting program: /bin/rm test

Program received signal SIGSEGV, Segmentation fault.
0x000172b4 in fts_open ()
(gdb) where
#0  0x000172b4 in fts_open ()
#1  0x00015cdc in ?? ()
Backtrace stopped: previous frame identical to this frame (corrupt stack?)
(gdb) ls
Undefined command: "ls".  Try "help".
(gdb) where
#0  0x000172b4 in fts_open ()
#1  0x00015cdc in ?? ()
Backtrace stopped: previous frame identical to this frame (corrupt stack?)
(gdb) list
1       ../sysdeps/unix/sysv/linux/arm/dl-procinfo.c: No such file or directory.
(gdb) 
```


### Strace output of the segfaulting command

```
execve("/bin/rm", ["rm", "stuff"], [/* 19 vars */]) = 0
brk(NULL)                               = 0x1e15000
uname({sysname="Linux", nodename="raspberrypi", ...}) = 0
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
access("/etc/ld.so.preload", R_OK)      = 0
open("/etc/ld.so.preload", O_RDONLY|O_CLOEXEC) = 3
fstat64(3, {st_mode=S_IFREG|0644, st_size=42, ...}) = 0
mmap2(NULL, 42, PROT_READ|PROT_WRITE, MAP_PRIVATE, 3, 0) = 0xb6f92000
close(3)                                = 0
open("/usr/lib/arm-linux-gnueabihf/libarmmem.so", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\1\1\1\0\0\0\0\0\0\0\0\0\3\0(\0\1\0\0\0\210\5\0\0004\0\0\0"..., 512) = 512
lseek(3, 20868, SEEK_SET)               = 20868
read(3, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"..., 1000) = 1000
lseek(3, 20540, SEEK_SET)               = 20540
read(3, "A,\0\0\0aeabi\0\1\"\0\0\0\0056\0\6\6\10\1\t\1\n\3\f\1\22\4\24"..., 45) = 45
fstat64(3, {st_mode=S_IFREG|0644, st_size=21868, ...}) = 0
mmap2(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0xb6f90000
mmap2(NULL, 86080, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0xb6f4d000
mprotect(0xb6f52000, 61440, PROT_NONE)  = 0
mmap2(0xb6f61000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x4000) = 0xb6f61000
close(3)                                = 0
munmap(0xb6f92000, 42)                  = 0
open("/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
fstat64(3, {st_mode=S_IFREG|0644, st_size=39495, ...}) = 0
mmap2(NULL, 39495, PROT_READ, MAP_PRIVATE, 3, 0) = 0xb6f86000
close(3)                                = 0
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
open("/lib/arm-linux-gnueabihf/libc.so.6", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\1\1\1\0\0\0\0\0\0\0\0\0\3\0(\0\1\0\0\0@h\1\0004\0\0\0"..., 512) = 512
lseek(3, 1231820, SEEK_SET)             = 1231820
read(3, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"..., 2880) = 2880
lseek(3, 1228284, SEEK_SET)             = 1228284
read(3, "A.\0\0\0aeabi\0\1$\0\0\0\0056\0\6\6\10\1\t\1\n\2\22\4\23\1\24"..., 47) = 47
fstat64(3, {st_mode=S_IFREG|0755, st_size=1234700, ...}) = 0
mmap2(NULL, 1303872, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0xb6e0e000
mprotect(0xb6f38000, 61440, PROT_NONE)  = 0
mmap2(0xb6f47000, 12288, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x129000) = 0xb6f47000
mmap2(0xb6f4a000, 9536, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0xb6f4a000
close(3)                                = 0
set_tls(0xb6f90ed0, 0xb6f915a8, 0xb6f94050, 0xb6f90ed0, 0xb6f94050) = 0
mprotect(0xb6f47000, 8192, PROT_READ)   = 0
mprotect(0xb6f4d000, 20480, PROT_READ|PROT_WRITE) = 0
mprotect(0xb6f4d000, 20480, PROT_READ|PROT_EXEC) = 0
cacheflush(0xb6f4d000, 0xb6f52000, 0, 0x15, 0) = 0
mprotect(0xb6f61000, 4096, PROT_READ)   = 0
mprotect(0x2b000, 4096, PROT_READ)      = 0
mprotect(0xb6f93000, 4096, PROT_READ)   = 0
munmap(0xb6f86000, 39495)               = 0
brk(NULL)                               = 0x1e15000
brk(0x1e36000)                          = 0x1e36000
open("/usr/lib/locale/locale-archive", O_RDONLY|O_LARGEFILE|O_CLOEXEC) = 3
fstat64(3, {st_mode=S_IFREG|0644, st_size=1679808, ...}) = 0
mmap2(NULL, 1679808, PROT_READ, MAP_PRIVATE, 3, 0) = 0xb6c73000
close(3)                                = 0
ioctl(0, TCGETS, {B115200 opost isig icanon echo ...}) = 0
--- SIGSEGV {si_signo=SIGSEGV, si_code=SEGV_MAPERR, si_addr=NULL} ---
+++ killed by SIGSEGV +++
```

#### Todo

~~1. Get Raspbian running in qemu-arm~~ Was not needed. Was able to compile and view the core on a differetn raspi system.   
~~2. Compile **/bin/rm** with symbols~~
~~3. Use GDB to run debug **/bin/rm** along with one of the core dumps. ~~
4. Generate a bug report 
5. ????
6. Ask for help from someone who knows Arm Assembly. 
7. Read up more on the strace output. 

#### Core Dump and rm with symbols viewed in GDB

Not sure what I am looking at, but this is a quick output of what I found when viewing the core using a /bin/rm that had debug symbols. 

```
GNU gdb (Raspbian 7.12-6) 7.12.0.20161007-git
Copyright (C) 2016 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "arm-linux-gnueabihf".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from rm...done.
[New LWP 31990]
Cannot access memory at address 0x7
Cannot access memory at address 0x3
Core was generated by `rm test'.
Program terminated with signal SIGSEGV, Segmentation fault.
#0  0x000172b4 in rpl_fts_open (argv=0x0, options=600, compar=0xbe90cea7) at lib/fts.c:439
439               if (! fts_palloc(sp, MAX(maxarglen, MAXPATHLEN)))
(gdb) list
434     #ifndef MAXPATHLEN
435     # define MAXPATHLEN 1024
436     #endif
437             {
438               size_t maxarglen = fts_maxarglen(argv);
439               if (! fts_palloc(sp, MAX(maxarglen, MAXPATHLEN)))
440                       goto mem1;
441             }
442
443             /* Allocate/initialize root's parent. */
(gdb) where
#0  0x000172b4 in rpl_fts_open (argv=0x0, options=600, compar=0xbe90cea7) at lib/fts.c:439
#1  0x00015cdc in freadahead (fp=0x1ceb860) at lib/freadahead.c:38
Backtrace stopped: previous frame identical to this frame (corrupt stack?)
(gdb)

```
