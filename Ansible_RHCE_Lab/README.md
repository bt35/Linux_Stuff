# Description: 

Let's setup a virtual lab, that can be wiped and recreated automagically. The goal is to have an environment that can be destroyed and reconfigured upon completion of any RHCE/RHCSA exercises. 

As an added bonus we get to learn how Ansible works with libvirt. 

#### Authors Note

The author is aware that his documentation is hard to follow, and he's never been particularly good at writing docs. But some documentation is better than none, ¯\\\_(ツ)\_/¯

## Lab Details

The virtual host, where all of the virts listed below will run, can be a laptop, or a desktop. In the authors case an external hard drive is used so that the Lab can be booted on any laptop or terminal, like dual booting. Simply plug in the laptop and change the IPL to boot the USB hard drive. 

The virtual host needs to have the following: 

* Apache installed, which will be used to host a local Centos 7 repository from which any virtual machines will be installed from. Also the kickstart configuration file that is used will reside there. 
* Firewall configurations to allow HTTP to pass between KVMs default network and the Virtual Host itself. 

The Lab will be setup with 3 virtual machines, configured via Kickstart, and static DCHP leases locked in by setting KVM to map ip addresses to each servers MAC address.  

1. KDC Server
 * Centos 7 
 * 20G XFS / lvm on vda
 * 5G Ext4 /local lvm on vdb
 * MAC: 52:54:00:66:3a:7c
2. NFS Server
 * Centos 7 
 * 20G XFS / 
 * 5G Ext4 /local
 * MAC: 52:54:00:66:3a:7b
3. NFS Client
 * Centos 7 
 * 20G XFS / 
 * 5g Ext4 /local 
 * MAC: 52:54:00:66:3a:7d

### Setting up the Virtual Host

Fedora 28 is a good Virtual Host for the purpose of this guide. Install Fedora 28 to a USB drive, use full disk encryption, and which ever desktop environment you like. 

#### Details Pertaining for Booting on Laptops that have Nvidia GPU

Any newer laptop with NVIDIA GPU will need to have the nouveau driver disabled as Centos/Rhel/Fedora will kernel panic. Please reboot after the driver has been blacklisted, otherwise **KVM Package installs may fail**. 

```
grubby --update-kernel ALL --args="rhgb quite"
grubby --update-kernel ALL --args="modeprobe.blacklist=nouveau"
grubby --info ALL | grep -i blacklist # To see the results of that.
reboot 
```

#### Install KVM and other necessities for Virtual Hosting

If 

```
 yum groupinstall "Virtualization Host" 
```

does not work, try the commands below:

```
yum install qemu-kvm qemu-img virt-manager libvirt libvirt-python libvirt-client virt-install virt-viewer bridge-utils
```

#### Install Ansible

Ansible is the automation tool that will be used to provision virtual machines, and configure the virtual machines. 

```
yum install ansible
```

#### Configure the Lab File Directory

The author used a non standard directory to store virtual disk images. The Qemu user needs permissions to access a nonstandard virtual disk storage directory, and that directories parents. Therefore a file ACL was used, that provides read and execute permissions. If less permissive file ACL is required remove the read permissions on the parent directory and only add an execute permission. 

```
sudo setfacl -R -m u:qemu:rx /home/<user>/lab_stuff
```

Directory Structure: This is important for use with the yaml files, as there are URIs that will need to be followed by ansible. 

```
[wololo@lab lab_stuff]$ ls -alt ./*
-rw-------. 1 root  root  54525952 Aug  4 18:21 ./CentOS-7-x86_64-DVD-1804.iso

./test:
total 36
-rw-rw-r--. 1 <user> <user>    10 Aug  4 21:43 virts.retry
-rw-r--r--. 1 <user> <user> 12288 Aug  4 21:43 .virts.yml.swp
drwxrwxr-x. 3 <user> <user>  4096 Aug  4 21:42 .
-rw-rw-r--. 1 <user> <user>  1476 Aug  4 21:42 virts.yml
drwxrwxr-x. 2 <user> <user>  4096 Aug  4 21:12 vars
drwxrwxr-x+ 4 <user> <user>  4096 Aug  4 20:25 ..

./virt_images:
total 12
drwxrwxr-x+ 4 <user> <user> 4096 Aug  4 20:25 ..
drwxr-xr-x. 2 root  root  4096 Aug  4 20:22 .
[wololo@lab lab_stuff]$
```

#### Configure an Apache Serve a Kickstart Configuration File and Serve a Local Centos Repository

Apache will be used to serve the kickstart files for each of the three servers detailed in the section called **Lab Details**. Also Apache will be used to serve a local Centos 7 repository, which will serve as install media for the virtual machines. Also the Centos 7 repo can serve as a local repository for the virtual machines once they are online. 

Install Apache:

```
sudo yum install httpd
sudo systemctl enable httpd
sudo systemctl start httpd
```

Make a root directory from which a kickstart file can be served as well as any other directory. Copy each of the kickstart files found in this repository to the /var/www/parent/ks directory. Do not change the file names or the virt.yml file will throw errors which say that ansible could not find the kickstart configurations. (The specific errors will be: undefined variable.) 

```
sudo mkdir -P /var/www/parent/ks
```

Configure a Local Centos Repo to be served by the HTTPd server.  

```
sudo mkdir -p /var/www/html/parent/repo
sudo rsync -avrt rsync://mirrors.ocf.berkeley.edu/centos/7/os/x86_64/ /var/www/html/parent/repo/CentOS/7.0/os/x86_64/ --progress --ignore-existing
```

Configure the firewall to allow traffic from virtual LAN to the Labs web server: 

```
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --reload
```

### Notes about the Kickstart Configuration Files: 

The root password is password. The test user password is password. This is a lab that should be running locally and be behind firewalld/iptables which should be running on the Virtual Host. 

The test users SSH public key will need to be to some key which **you** have created.

The kickstart files setup local domain name resolution for the lab using /etc/hosts, it sets the hostname of each server, provisions the two virtual disks attached to the server, sets the root users password, adds the test user, and adds the test users ssh key.

*The Kickstart even installs VIM for you*

Here is the Kickstart Config for the KDC Server: 

```
text
install
lang en_US.UTF-8
keyboard us
network --device eth0 --onboot no --bootproto dhcp --hostname kdc-server.nonsense.org
timezone America/NewYork
auth --useshadow --enablemd5
selinux --disabled
firewall --disabled
services --enabled=NetworkManager,sshd
eula --agreed
reboot
ignoredisk --only-use=vda,vdb

bootloader --location=mbr
zerombr
clearpart --all --initlabel
part swap --asprimary --fstype="swap" --size=1024 --ondisk=vda
part /boot --fstype xfs --size=200 --ondisk=vda
part pv.01 --size=1 --grow --ondisk=vda
volgroup rootvg01 pv.01
logvol / --fstype xfs --name=lv01 --vgname=rootvg01 --size=1 --grow

#
part pv.02 --size=1 --grow --ondisk=vdb
volgroup localvg01 pv.02
logvol /local --fstype ext4 --name=lv01 --vgname=localvg01 --size=1 --grow

rootpw --iscrypted $1$gMnWl4Ec$Z55RlGBxEqWa2GB/71iTg1

# Create second user...
user --name=test --groups=users,wheel --password=password
# add ssh key for test
sshkey --username=test "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCrb8jOUKWQuYCChkAZHn2rmwExQfIynl8fXTy2ePg7B1x0JlxGXahGAS2QFo0MV6Z5zzJpLodvM9J+ClTYM6PHz7SIALoNvCGWwejhe6QaDfP/S1HvBrB6o8sDPWNotRhIkunbPe2EzIDI7FpW/tiWRY4JBJoNjhRDAQhg5VBb+uhbRGRNQEPXfJALg0hfyq/ab0Di0v0YsSmiLUdClDBbSskESdPHNfWldH2SIZuQt541Qw0bcXqOR+ed7pWkj4pxVZrJNW+Fx5nEd03xXQCizQcnuVO7yG1YjET3Rxg8rwRhikGrqPjOHoRk3zVZkaqFK0CGMk8uPrLGdZSZW7tzpWQpcLUtcSnBKHmlMqbhNIrY38jYUBdQSmOkBl6w+O1i+zYC4ilN1ebdNpIfaT3Lvz+m8PEmqoNu1hNzxXRxKz8vIZLcbHisURxXjLPIjOAXPFdTY2P+VFBKiXFvcaaoPnCg3V2M6sv8jy0z28jz6kajZODqoY2Vk+xcRygx0JRvjlTk35/gifyld6Y0mzb0QN6YWhe8K8AWF1fLvE+8P8TZZ/d/6oOMsMfJGjuc0UwUjAmtavydEh9aMpJGrePLvkoieF6qHi1UiNiAsrlFNl63zN1xlmp6YFYQ6OmpC5OERUNhRivDLEQ3pycZgNbcnnp1JYV91rOhTRks2FnLjQ== test@lab.somenonsense.org"


repo --name=base --baseurl=http://192.168.122.1/parent/repo/CentOS/7.0/os/x86_64/
url --url="http://192.168.122.1/parent/repo/CentOS/7.0/os/x86_64/"

# Install and configure ssh?

%packages --nobase --ignoremissing
@core
ssh*
vim
%end

%post
echo "192.168.122.2 kdc-server.nonsense.org" >> /etc/hosts
echo "192.168.122.3 nfs-client.nonsense.org" >> /etc/hosts
echo "192.168.122.4 nfs-server.nonsense.org" >> /etc/hosts
echo "192.168.122.1 repo.nonsense.org" >> /etc/hosts
%end

```


### Create an Ansible Playbook for creating Virtual Machines

This playbook uses jinja2 templating and the **items** are pulled from the vars_file. The Ansible task called create-vm uses virt-install as a shell command to iterate over the **vars/guest.yml** template using the **items** variable. This task essentially kicks off three separate Centos virtual machines, which are configured to boot with a kickstart configuration and those kickstart config files should be found on the Apache web server which was installed in the above sections. Once again, the kickstart config files, and centos repo live on the virtual host. 

```
---
- name: manage libvirt guests
  become: true
  become_user: root
  become_method: sudo
  hosts: 127.0.0.1

  vars_files:
      - vars/guests.yml

  tasks:
      - name: start libvirtd
        service: name=libvirtd state=started enabled=yes
        register: libvirtd

      - name: wait for libvirtd to get up
        pause: seconds=30
        when: libvirtd.changed

      - name: get list of vms
        virt: command=list_vms
        register: virt_vms

      - name: Any Virts
        debug:
        msg: "Virts...: {{ virt_vms }}"

      - name: create vm
        command: virt-install -n {{item.name}}
                 -r {{ item.mem }}
                 --vcpus {{ item.cpu }}
                 --location={{ item.url }}
                 --os-type {{ item.os.type }}
                 --os-variant {{ item.os.variant }}
                 --extra-args="ks={{ item.ks }}.cfg"
                 --network network=default
                 --mac={{ item.mac }}
                 --{{item.virt_hypervisor}}
                 --virt-type {{ item.virt_type }}
                 --disk size={{item.disk.size}},path={{item.disk.path}}/{{item.name}}_{{item.disk.name}}.img
                 --disk size={{item.disk1.size}},path={{item.disk1.path}}/{{item.name}}_{{item.disk1.name}}.img
                 --noautoconsole
        when: item.name not in virt_vms.list_vms
        with_items: "{{ guests }}"

      - name: get guest info
        virt: command=info
        register: virt_info

      - name: make sure all vms are running
        virt: name={{item.name}} command=start
        when: virt_info[item.name]['state'] != 'running'
        with_items: guests


```


### Configure the Ansible Variables file for the Virtual Machines

Setup a "guests.yml" file. Which should contain details about each virtual machine to create. This file should reside in the var/ directory, and that var/ directory should reside in the same directory as the virt.yml Ansible playbook described in the section above. **Change the disk section to store the virtual machine images to which ever directory you chose in the Lab File Directory section**. 

Note that the **url**: points to what should be an Apache web server that is running on the Virtual Host. 

```
---
guests:

    - name: centos7_KDC
      url: http://192.168.122.1/parent/repo/CentOS/7.0/os/x86_64/
      cpu: 1
      mem: 2000
      mac: 52:54:00:66:3a:7c
      virt_type: kvm
      virt_hypervisor: hvm
      ks: http://192.168.122.1/parent/ks/ks_kdc
      os:
          type: linux
          variant: rhel7
      disk:
          size: 20
          path: /home/<user>/lab_stuff/virt_images
          name: disk1
      disk1:
          size: 5
          path: /home/<user>/lab_stuff/virt_images
          name: disk2

    - name: centos7_Kerb_NFS_Client
      url: http://192.168.122.1/parent/repo/CentOS/7.0/os/x86_64/
      cpu: 1
      mem: 2000
      mac: 52:54:00:66:3a:7b
      virt_type: kvm
      virt_hypervisor: hvm
      ks: http://192.168.122.1/parent/ks/ks_nfs_client
      os:
          type: linux
          variant: rhel7
      disk:
          size: 20
          path: /home/<user>/lab_stuff/virt_images
          name: disk1
      disk1:
          size: 5
          path: /home/<user>/lab_stuff/virt_images
          name: disk2

    - name: centos7_File_Server
      url: http://192.168.122.1/parent/repo/CentOS/7.0/os/x86_64/
      cpu: 1
      mem: 2000
      mac: 52:54:00:66:3a:7d
      virt_type: kvm
      virt_hypervisor: hvm
      ks: http://192.168.122.1/parent/ks/ks_nfs_server
      os:
          type: linux
          variant: rhel7
      disk:
          size: 20
          path: /home/<user>/lab_stuff/virt_images
          name: disk1
      disk1:
          size: 5
          path: /home/<user>/lab_stuff/virt_images
          name: disk2
```

#### Configure LibVirt to Statically Assign DCHP Leases to each Virtual Machine

As stated in the **Lab Description** section above, each server has a MAC address assigned to it. That MAC address is locally unique and it should be used to assign static DHCP leases, using virsh. 

On the virtual host use virsh to net-edit and add the static DHCP lease assignments, see below. 

**Note:** Using net edit in virsh will open the network xml file in VI/VIM, make edits and then press :wq, other wise no changes will be made. 
**Note:** If the virt install process was started before this step, they will need to be re-provisioned. 

```
sudo virsh
virsh # net-edit default
<network>
  <name>default</name>
  <uuid>0e6ac5ae-5e5f-443a-b7cf-fb0d040bca89</uuid>
  <forward mode='nat'/>
  <bridge name='virbr0' stp='on' delay='0'/>
  <mac address='52:54:00:84:c7:30'/>
  <ip address='192.168.122.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.122.2' end='192.168.122.254'/>
      <host mac='52:54:00:66:3a:7c' name='cento7_Repo_KDC' ip='192.168.122.2'/>
      <host mac='52:54:00:66:3a:7b' name='centos7_Kerb_Client' ip='192.168.122.3'/>
      <host mac='52:54:00:66:3a:7d' name='centos7_File_Server' ip='192.168.122.4'/>
    </dhcp>
  </ip>
</network>

```

### Run the Ansible Provisioning Process

Run the following playbook command to star the install process. 

```
ansible-playbook -K virts.yml
```

Note that the last task in the **virts.yml** playbook will fail, because the virtual machines will not be online fast enough for the task to complete successfully. They take a moment to create and boot, due to the disk allocation. Feel free to add a delay. 

Once the **create_vm** task is complete you can open the **virt manager** GUI and watch the Centos 7 install process. Provided the kickstart and Repo files are where need to be, see the **Apache** section, the virts should install. 

Note, SELinux is not enabled. The servers should reboot after post installation configuration. 


#### Troubleshooting

- It may not be apparent from the TTY console that the servers are actually downloading the centos installation files from the mirror hosted by VCU (http://mirror.vcu.edu/pub/gnu_linux/centos/7/os/x86_64/), so run the following command to see:
- The ip address 128.172.15.65 is the VCU FTP/HTTP repo. 

```
sudo tcpdump -i virbr0
23:48:07.237800 IP 192.168.122.175.56352 > 128.172.15.65.http: Flags [.], ack 279072, win 3611, options [nop,nop,TS val 4294831514 ecr 2621570695,nop,nop,sack 1 {280440:310536}], length 0
23:48:07.237800 IP 192.168.122.58.55746 > 128.172.15.65.http: Flags [.], ack 5303736, win 6647, options [nop,nop,TS val 4294789315 ecr 2621570932], length 0
23:48:07.237805 IP 192.168.122.186.50070 > 128.172.15.65.http: Flags [.], ack 5075280, win 6075, options [nop,nop,TS val 4294871459 ecr 2621570939], length 0
23:48:07.237818 IP 192.168.122.58.55746 > 128.172.15.65.http: Flags [.], ack 5306472, win 6647, options [nop,nop,TS val 4294789315 ecr 2621570939], length 0
23:48:07.237821 IP 192.168.122.186.50070 > 128.172.15.65.http: Flags [.], ack 5078016, win 6075, options [nop,nop,TS val 4294871459 ecr 2621570939], length 0
23:48:07.237831 IP 192.168.122.58.55746 > 128.172.15.65.http: Flags [.], ack 5309208, win 6647, options [nop,nop,TS val 4294789315 ecr 2621570939], length 0
^C
7601 packets captured
```

- The kickstart file is served on the lab local host. Therefore the virts can only reach it via the virbr0 ip address: 192.168.122.1/ks/ks.cfg.

- The virts each needed 2000 MB of memory. The install process is taking place. 

- Current problem is, no route to virbr0 virtual host at 192.168.122.1. Therefore anaconda cannot get the kickstart configuration. 
- The fix was 

```
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --reload
```
**If you have files not found or variables not found, check the directory structure or URI of the kickstart and vars/guest.yml files** 

### Credits
  * Credit to Vivek Gite for the KVM Kickstart guide here: https://www.cyberciti.biz/faq/kvm-install-centos-redhat-using-kickstart-ks-cfg/
  * Credit to the author of this guide: https://mangolassi.it/topic/15257/ansible-create-kvm-guests
  * Credit to https://github.com/sfromm/ansible-playbooks, for the vars/guest.yml and virt.yml. 

## TODO: 

1. Proof read this... not just for spelling but continuity. 
2. Use an Ansible role to configure the virtualmachines instead of Kickstart. 

~~3. Configure static IP addresses for each virtual machine.~~ 

~~4. Configure the guest var, and Ansible to create a second disk for one or all of the virtual machines.~~
