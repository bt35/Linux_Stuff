## Testing Stress testing. 

The virt has the following settings: 

```
free -m
              total        used        free      shared  buff/cache   available
Mem:            985          72         647           0         266         774
Swap:           947           0         947


processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 94
model name	: Intel Core Processor (Skylake, IBRS)
stepping	: 3
microcode	: 0x1
cpu MHz		: 2400.000
cache size	: 16384 KB
physical id	: 0
siblings	: 1
core id		: 0
cpu cores	: 1
apicid		: 0
initial apicid	: 0
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch cpuid_fault invpcid_single pti ibrs ibpb fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 xsaves arat
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs
bogomips	: 4800.00
clflush size	: 64
cache_alignment	: 64
address sizes	: 40 bits physical, 48 bits virtual
power management:

processor	: 1
vendor_id	: GenuineIntel
cpu family	: 6
model		: 94
model name	: Intel Core Processor (Skylake, IBRS)
stepping	: 3
microcode	: 0x1
cpu MHz		: 2400.000
cache size	: 16384 KB
physical id	: 1
siblings	: 1
core id		: 0
cpu cores	: 1
apicid		: 1
initial apicid	: 1
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch cpuid_fault invpcid_single pti ibrs ibpb fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 xsaves arat
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs
bogomips	: 4800.00
clflush size	: 64
cache_alignment	: 64
address sizes	: 40 bits physical, 48 bits virtual
power management:

```

## Test Run 1. 

Two workers at 100m. 

stress-ng --vm 2 --vm-bytes 100m

**dstat output** 

The run column shows the nubmer of processes that the user is running. When it hits 2 this shows that stress-ng started. It takes a few cycles before the memory usage evens out on the lower memory usage. 

```
stressball@stressedout:~/testStress/memory100mTest$ dstat --vmstat -s
---procs--- ------memory-usage----- ---paging-- -dsk/total- ---system-- --total-cpu-usage-- ----swap---
run blk new| used  free  buff  cach|  in   out | read  writ| int   csw |usr sys idl wai stl| used  free
  0   0 0.9|44.5M  902M 4744k 21.5M| 257k  287k| 445k  361k| 138   154 |  8   1  90   1   0|  34M  913M
  0   0   0|44.5M  902M 4744k 21.5M|   0     0 |   0     0 |  40    66 |  0   0 100   0   0|  34M  913M
  0   0   0|44.5M  902M 4744k 21.5M|   0     0 |   0     0 |  46    66 |  1   0 100   0   0|  34M  913M
  0   0   0|44.5M  902M 4744k 21.5M|   0     0 |   0     0 |  37    59 |  1   0  99   0   0|  34M  913M
  0   0   0|44.5M  902M 4744k 21.5M|   0     0 |   0     0 |  56   119 |  0   0 100   0   0|  34M  913M
2.0   0 5.0| 147M  796M 4752k 27.0M| 264k    0 |3616k   16k| 623   421 | 66  19  14   0   0|  34M  913M
2.0   0   0| 146M  797M 4756k 27.0M|   0     0 |4096B 8192B| 531   150 | 83  17   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4756k 27.0M|   0     0 |   0     0 | 511    61 | 98   2   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4756k 27.0M|   0     0 |   0     0 | 516    57 | 97   3   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4756k 27.0M|   0     0 |   0     0 | 510    35 |100   0   0   0   0|  34M  913M
3.0   0   0| 148M  796M 4756k 27.0M|   0     0 |   0     0 | 515    54 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4764k 27.0M|   0     0 |   0    36k| 516    56 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4764k 27.0M|   0     0 |   0     0 | 511    38 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4764k 27.0M|   0     0 |   0     0 | 516    70 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4764k 27.0M|   0     0 |   0     0 | 513    54 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4764k 27.0M|   0     0 |   0     0 | 514    56 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4764k 27.0M|   0     0 |   0    36k| 518    48 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4764k 27.0M|   0     0 |   0     0 | 510    42 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4764k 27.0M|   0     0 |   0     0 | 512    45 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4764k 27.0M|   0     0 |   0     0 | 514    57 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  796M 4764k 27.0M|   0     0 |   0     0 | 514    47 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  795M 4764k 27.0M|   0     0 |   0     0 | 510    32 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  795M 4764k 27.0M|   0     0 |   0     0 | 511    49 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  795M 4764k 27.0M|   0     0 |   0     0 | 514    43 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  795M 4764k 27.0M|   0     0 |   0     0 | 509    31 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  795M 4764k 27.0M|   0     0 |   0     0 | 512    51 |100   0   0   0   0|  34M  913M
2.0   0   0| 148M  795M 4764k 27.0M|   0     0 |   0     0 | 516    73 |100   0   0   0   0|  34M  913M
  0   0   0|45.4M  898M 4764k 24.9M|   0     0 |   0     0 | 150   105 | 13   1  86   0   0|  34M  913M^
```

## Test Run 1. 

Two workers at 500m . 

stress-ng --vm 2 --vm-bytes 500m

**dstat output** 

```
stressball@stressedout:~/testStress/memory100mTest$ dstat --vmstat -s
---procs--- ------memory-usage----- ---paging-- -dsk/total- ---system-- --total-cpu-usage-- ----swap---
run blk new| used  free  buff  cach|  in   out | read  writ| int   csw |usr sys idl wai stl| used  free
  0   0 0.9|44.7M  898M 4772k 24.9M| 247k  275k| 429k  346k| 141   150 |  9   1  89   1   0|  34M  913M
  0   0   0|44.7M  898M 4772k 24.9M|   0     0 |   0     0 |  63    93 |  0   1 100   0   0|  34M  913M
  0   0   0|44.7M  898M 4772k 24.9M|   0     0 |   0     0 |  49    81 |  0   0 100   0   0|  34M  913M
  0   0   0|44.7M  899M 4772k 24.9M|   0     0 |   0     0 |  47   100 |  1   0  99   0   0|  34M  913M
  0   0   0|44.7M  899M 4772k 24.9M|   0     0 |   0     0 |  66   115 |  1   0  99   0   0|  34M  913M
2.0   0 5.0| 548M  395M 4772k 27.3M|   0     0 | 272k    0 | 389   119 | 58  12  30   0   0|  34M  913M
2.0   0   0| 548M  395M 4772k 27.3M|   0     0 |   0     0 | 505    29 |100   1   0   0   0|  34M  913M
2.0   0   0| 371M  572M 4772k 27.3M|   0     0 |   0     0 | 518   141 | 68  33   0   0   0|  34M  913M
2.0   0   0| 490M  453M 4772k 27.3M|   0     0 |   0     0 | 511    69 | 80  20   0   0   0|  34M  913M
2.0   0   0| 383M  560M 4772k 27.3M|   0     0 |   0     0 | 512    54 | 89  12   0   0   0|  34M  913M
2.0   0   0| 286M  656M 4780k 27.2M|   0     0 |   0    20k| 516    69 | 85  15   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 513    99 | 75  25   0   0   0|  34M  913M
2.0   0   0| 613M  330M 4780k 27.3M|   0     0 |   0    12k| 508    41 | 87  13   0   0   0|  34M  913M
2.0   0   0| 613M  330M 4780k 27.3M|   0     0 |   0     0 | 503    31 |100   0   0   0   0|  34M  913M
2.0   0   0| 613M  330M 4780k 27.3M|   0     0 |   0     0 | 509    49 |100   0   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 509    53 | 94   6   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 514    51 | 94   6   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 509    56 |100   0   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 503    25 |100   0   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 502    23 |100   0   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 510    51 |100   0   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 512    62 | 85  15   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 507    45 |100   0   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 511    53 |100   0   0   0   0|  34M  913M
2.0   0   0| 551M  392M 4780k 27.3M|   0     0 |   0     0 | 507    57 |100   0   0   0   0|  34M  913M
  0   0   0|47.4M  895M 4780k 25.1M|  20k    0 |  20k    0 | 233   123 | 31   3  67   0   0|  34M  913M^C
```

## Test Run 3. 

Two workers at 800m . 

stress-ng --vm 2 --vm-bytes 800m

**dstat output** 

For this run we can see that a little bit after stress-ng starts the virt starts to swap which is indicated by the disk IO and swap usage on the far right column. 

```
stressball@stressedout:~/testStress/memory100mTest$ dstat --vmstat -s
---procs--- ------memory-usage----- ---paging-- -dsk/total- ---system-- --total-cpu-usage-- ----swap---
run blk new| used  free  buff  cach|  in   out | read  writ| int   csw |usr sys idl wai stl| used  free
  0   0 0.9|46.9M  896M 4788k 25.1M| 239k  266k| 415k  335k| 144   146 | 10   1  88   1   0|  34M  913M
  0   0   0|46.9M  896M 4788k 25.1M|   0     0 |   0     0 |  53    79 |  0   0 100   0   0|  34M  913M
  0   0   0|46.9M  896M 4788k 25.1M|   0     0 |   0     0 |  72   116 |  0   0  99   0   0|  34M  913M
  0   0   0|46.9M  896M 4788k 25.1M|   0     0 |   0     0 |  49    82 |  1   0  99   0   1|  34M  913M
  0   0   0|46.8M  896M 4788k 25.1M|   0     0 |   0     0 |  45    81 |  0   1  99   0   0|  34M  913M
2.0   0 5.0| 800M  142M 4788k 27.5M|   0     0 | 228k    0 | 410   109 | 58  18  25   0   0|  34M  913M
2.0   0   0| 851M 91.7M 4788k 27.5M|   0     0 |   0    72k| 513    44 | 99   2   0   0   0|  34M  913M
2.0   0   0| 851M 91.7M 4788k 27.5M|   0     0 |   0     0 | 502    25 |100   0   0   0   0|  34M  913M
2.0   0   0| 112M  831M 4788k 27.5M|   0     0 |   0     0 | 521   120 | 49  51   0   0   0|  34M  913M
2.0   0   0| 851M 91.8M 4788k 27.5M|   0     0 |   0     0 | 505    37 | 86  14   0   0   0|  34M  913M
2.0   0   0| 851M 91.6M 4788k 27.5M|   0     0 |   0     0 | 508    53 | 80  20   0   0   0|  34M  913M
2.0   0   0| 851M 91.6M 4796k 27.5M|   0     0 |   0    20k| 510    61 | 77  23   0   0   0|  34M  913M
2.0   0   0| 309M  634M 4796k 27.5M|   0     0 |   0     0 | 508    59 | 91  10   0   0   0|  34M  913M
2.0   0   0| 851M 91.6M 4796k 27.5M|   0     0 |   0     0 | 505    37 | 86  15   0   0   0|  34M  913M
2.0   0   0| 851M 91.6M 4796k 27.5M|   0     0 |   0     0 | 505    43 | 80  20   0   0   0|  34M  913M
2.0   0   0| 529M  413M 4796k 27.5M|   0     0 |   0     0 | 518   100 | 64  37   0   0   0|  34M  913M
2.0   0   0| 247M  696M 4796k 27.5M|   0     0 |   0     0 | 510    55 | 89  12   0   0   0|  34M  913M
2.0   0   0| 908M 58.8M  644k 8012k|  20M   62M|  31M   62M|7507  3141 | 51  34  10   5   0|  95M  852M
2.0   0   0| 891M 76.7M  600k 7124k|  31M   46M|  31M   46M|6800  2600 | 79  11   0  10   1| 141M  806M
4.0   0   0| 913M 56.0M  148k 5520k|  40M   16M|  43M   16M|7021  9917 | 67  13   4  15   1| 144M  803M
2.0   0   0| 902M 66.2M  148k 4076k|  66M   47M|  74M   48M|  12k   18k| 51  21   4  23   1| 145M  802M
2.0   0   0| 898M 68.4M  148k 6812k|  10M    0 |  12M    0 |1248   873 | 93   3   0   4   1| 144M  803M
3.0   0   0| 905M 62.4M  148k 4868k|  11M    0 |  11M    0 | 999   859 | 93   4   0   3   0| 144M  803M
2.0   0   0| 900M 69.1M  148k 3360k|  39M   11M|  43M   11M|5675  6307 | 65  19   3  13   0| 145M  802M
2.0   0   0| 578M  387M  148k 7612k|  11M    0 |  15M    0 |1326  1279 | 92   4   0   4   0|  94M  853M
2.0   0   0| 839M  126M  148k 7560k|  48k    0 |  48k    0 | 504    55 | 84  16   0   0   0|  44M  903M
2.0   0   0| 839M  126M  148k 7632k| 124k    0 | 208k    0 | 541    67 |100   0   0   0   0|  44M  903M
2.0   0   0| 840M  125M  648k 7796k| 204k    0 | 892k    0 | 547   230 | 81  19   0   0   0|  44M  903M
  0   0   0|39.0M  924M  656k 9244k| 888k    0 |2376k    0 | 690   332 | 95   3   2   1   0|  41M  906M
  0   0   0|38.9M  924M  656k 9300k|   0     0 |  40k    0 |  39    64 |  0   0 100   0   0|  41M  906M^C

```

## Test Run 4. 

Two workers at 800m . 

stress-ng --vm 2 --vm-bytes 900m

**dstat output** 

Very busy now...

```
stressball@stressedout:~/testStress/memory100mTest$ dstat --vmstat -s
---procs--- ------memory-usage----- ---paging-- -dsk/total- ---system-- --total-cpu-usage-- ----swap---
run blk new| used  free  buff  cach|  in   out | read  writ| int   csw |usr sys idl wai stl| used  free
  0   0 0.8|42.2M  912M 2524k 16.2M| 363k  357k| 552k  420k| 165   165 | 10   1  88   1   0|  37M  911M
1.0   0   0|42.2M  912M 2524k 16.2M|  24k    0 |  24k    0 |  83   131 |  1   0  99   0   0|  37M  911M
  0   0   0|42.2M  912M 2524k 16.2M|  12k    0 |  12k    0 |  46    73 |  0   0 100   0   0|  37M  911M
  0   0   0|42.3M  912M 2524k 16.2M|  40k    0 |  40k    0 |  61   111 |  1   0  99   0   0|  37M  911M
1.0   0 1.0|43.0M  910M 2532k 16.8M| 272k    0 |1012k    0 | 116   228 |  1   1  98   1   0|  37M  911M
2.0   0 4.0| 901M 68.3M  136k 5664k|  26M   70M|  33M   70M|7810  2592 | 60  38   0   1   0|  83M  864M
2.0 1.0   0| 900M 69.0M  152k 5492k| 241M  240M| 247M  240M|  28k   18k| 18  49  12  20   1|  83M  864M
2.0 1.0   0| 901M 67.9M  152k 5860k| 255M  255M| 262M  256M|  26k   21k| 16  44   7  31   1|  82M  865M
2.0 1.0   0| 910M 59.1M  152k 5864k| 263M  255M| 271M  255M|  28k   22k| 13  50   6  31   0|  75M  873M
4.0   0   0| 912M 57.8M  152k 3188k| 247M  245M| 255M  245M|  24k   24k| 14  40   8  38   1|  74M  874M
2.0 2.0   0| 905M 63.6M  152k 3784k| 178M  232M| 185M  221M|  20k   18k|  3  12   3  79   3| 125M  822M missed 2 ticks
2.0 2.0   0| 892M 75.6M  152k 4952k|  61M   28M|  63M   39M|5116  6998 | 10  20   5  53  12|  94M  854M
2.0   0   0| 908M 61.2M  140k 3392k| 227M  210M| 232M  211M|  24k   21k| 15  41  13  30   1|  76M  871M
  0 2.0   0| 885M 84.6M  140k 3372k| 213M  236M| 219M  236M|  31k   17k| 16  44  24  15   1|  98M  849M
1.0 1.0   0| 905M 63.8M  140k 3428k| 246M  226M| 255M  226M|  27k   20k| 13  45  31  11   0|  78M  869M
3.0   0   0| 902M 67.1M  152k 3336k| 251M  254M| 258M  254M|  30k   19k| 17  44  28  10   1|  81M  866M
2.0   0   0| 899M 68.8M  144k 4448k| 238M  241M| 245M  241M|  27k   20k| 12  46  30  12   1|  83M  864M
1.0 1.0   0| 910M 59.7M  144k 3392k| 230M  220M| 236M  220M|  25k   20k| 18  40  30  12   1|  73M  874M
1.0 1.0   0| 907M 62.4M  144k 3408k| 244M  247M| 250M  247M|  28k   21k| 16  41  30  12   1|  76M  871M
1.0 1.0   0| 910M 59.4M  144k 3412k| 212M  210M| 219M  210M|  18k   19k| 15  33  34  16   3|  73M  874M
1.0 2.0   0| 906M 63.9M  144k 3296k| 194M  201M| 202M  200M|  16k   17k| 12  35  34  15   4|  79M  868M
2.0 1.0   0| 906M 63.8M  144k 3300k| 235M  234M| 241M  234M|  23k   21k| 13  39  33  14   0|  78M  869M
2.0 1.0   0| 905M 64.3M  156k 3372k| 225M  226M| 236M  225M|  22k   21k| 13  39  33  14   1|  78M  870M
3.0 1.0   0| 904M 64.9M  156k 3428k| 228M  230M| 234M  230M|  22k   27k| 14  37  11  37   1|  79M  868M
2.0 1.0   0| 912M 57.3M  156k 3280k| 245M  237M| 253M  237M|  25k   22k| 13  41  32  13   1|  71M  876M
1.0 1.0   0| 906M 63.4M  156k 3352k| 222M  228M| 228M  229M|  23k   20k| 14  38  33  15   1|  77M  870M
  0   0   0|38.1M  927M  156k 8136k| 191M  187M| 204M  187M|  21k   18k| 12  36  40  11   0|  41M  906M
  0   0   0|38.2M  926M  156k 8632k|   0     0 | 240k    0 |  53    55 |  0   1  99   0   0|  41M  906M
  0   0   0|38.5M  924M  636k 9616k| 204k    0 |1664k    0 | 119   416 |  1   1  98   1   0|  41M  906M
---procs--- ------memory-usage----- ---paging-- -dsk/total- ---system-- --total-cpu-usage-- ----swap---
run blk new| used  free  buff  cach|  in   out | read  writ| int   csw |usr sys idl wai stl| used  free
  0   0   0|38.6M  924M  636k 9616k| 136k    0 | 140k    0 |  78   134 |  0   0  99   0   0|  41M  906M^C

```

## Test Run 4. 

Two workers at 1000. 

stress-ng --vm 2 --vm-bytes 1000m

**dstat output** 

Started to really eat into the swap space and managed to use up enough memory to trigger stress-ng mmap to fail. [*]

[*]
```
stress-ng: error: [1560] stress-ng-vm: gave up trying to mmap, no available memory
^Cstress-ng: info:  [1555] successful run completed in 21.42s
```

```
stressball@stressedout:~/testStress/memory100mTest$ dstat --vmstat -s
---procs--- ------memory-usage----- ---paging-- -dsk/total- ---system-- --total-cpu-usage-- ----swap---
run blk new| used  free  buff  cach|  in   out | read  writ| int   csw |usr sys idl wai stl| used  free
  0   0 0.6|43.3M  913M 2552k 14.2M|9123k   10M|9615k   10M|1128   875 |  7   3  86   4   0|  40M  907M
  0   0   0|43.3M  913M 2552k 14.2M|4096B    0 |4096B    0 |  73    97 |  1   0  99   0   1|  40M  907M
  0   0   0|43.3M  913M 2552k 14.3M|  40k    0 | 128k    0 |  67    99 |  0   0 100   0   0|  40M  907M
  0   0   0|43.3M  913M 2552k 14.3M|  24k    0 |  24k    0 |  59   106 |  0   0  99   0   0|  40M  907M
  0   0   0|43.3M  913M 2552k 14.3M|8192B    0 |8192B    0 |  40    59 |  0   0 100   0   0|  40M  907M
2.0   0 5.0| 641M  311M 2556k 20.8M| 528k    0 |4912k    0 | 535   545 | 43  18  37   1   0|  40M  907M
3.0   0   0| 922M 47.8M  148k 5548k|1024k  387M|4652k  387M|  48k 2411 | 29  37   7  18   9| 429M  519M
  0 3.0   0| 919M 48.9M  160k 7460k| 272k  140M|4456k  138M|  18k 1016 |  4   8   1  86   1| 566M  381M
3.0 3.0   0| 908M 59.0M  564k 8324k| 140k  776k|   0     0 |  54    17 | 21  36   0   0  43| 573M  374M missed 3 ticks
3.0 1.0   0| 916M 50.4M  708k 8432k| 100k   71M| 684k   72M|8036   304 | 23  40   0   0  37| 637M  310M
3.0 1.0   0| 917M 50.4M  648k 5576k|1344k  286M|1504k  286M|  36k  514 | 31  49   5   7   9| 921M   26M
  0 2.0 1.0| 577M  389M  284k 7336k| 116M   45M| 123M   46M|  12k   15k| 12  24  24  38   3| 414M  533M
1.0 1.0   0| 716M  247M  288k 9.80M| 139M    0 | 142M    0 |8892    18k|  6  11  52  30   0| 275M  672M
  0 2.0   0| 868M 95.0M  292k 9.84M| 152M    0 | 152M    0 |9693    19k|  6  11  53  30   0| 123M  824M
1.0 1.0   0| 885M 78.8M  268k 8744k|  47M   30M|  47M   30M|8325  6534 |  5  10   0  75  11| 108M  839M
2.0 1.0   0| 903M 66.4M  156k 3340k|  66M   49M|  70M   49M|8593  6013 | 10  18   0  68   4|  89M  858M
1.0 1.0   0| 894M 74.6M  156k 4368k| 103M  112M| 109M  112M|  18k 7142 |  9  33   0  57   1|  97M  850M
1.0 1.0   0| 896M 72.1M  156k 4356k| 166M  160M| 171M  160M|  25k   11k|  9  30   0  60   1|  92M  855M
1.0 1.0   0| 889M 80.6M  160k 3448k|  96M  103M| 102M  103M|  19k 6818 | 24  24   0  47   6|  99M  848M
  0 2.0   0| 864M  104M  148k 4336k| 104M  128M| 111M  128M|  17k 7240 | 27  23   0  51   0| 123M  824M
1.0 1.0   0| 899M 68.4M  148k 5208k| 122M   87M| 125M   87M|  16k 8502 | 19  16   0  59   5|  88M  859M
2.0   0   0| 908M 60.7M  144k 4160k| 182M  174M| 199M  174M|  28k   13k| 10  31  25  33   0|  81M  866M
  0 1.0   0| 892M 76.9M  132k 4176k| 180M  195M| 187M  196M|  31k   12k| 10  34  32  23   0|  96M  851M
1.0   0   0| 906M 62.5M  312k 4212k| 161M  147M| 167M  147M|  25k   11k| 13  28  36  22   1|  82M  865M
  0   0   0|39.0M  927M  640k 6616k| 137M  145M| 141M  145M|  23k 9858 |  8  30  44  18   0|  44M  903M
  0   0   0|38.9M  926M  640k 7516k|4096B    0 | 904k    0 |  81   108 |  0   1  99   0   0|  44M  903M^C
```

### The very high test runs triggered OOM killer: 

OOM Killer kills processes based on a badness score. Important programs are given a lower score like kernel services. Badness is calculated by number of child processes and memory usage. 

```
...
[ 2784.679017] Out of memory: Kill process 1566 (stress-ng-vm) score 1462 or sacrifice child
[ 2784.679062] Killed process 1566 (stress-ng-vm) total-vm:1026600kB, anon-rss:450804kB, file-rss:0kB, shmem-rss:0kB
[ 2784.733529] oom_reaper: reaped process 1566 (stress-ng-vm), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
[ 2816.870035] stress-ng-vm invoked oom-killer: gfp_mask=0x14280ca(GFP_HIGHUSER_MOVABLE|__GFP_ZERO), nodemask=(null), order=0, oom_score_adj=1000
[ 2816.870037] stress-ng-vm cpuset=/ mems_allowed=0
...
# Badness score can be seen here, 1000 is pretty high. 
[ 2889.077577] [ 1588]  1000  1588   397450   163169  2416640   124159          1000 stress-ng-vm
[ 2889.077582] [ 1589]  1000  1589   397450    56809  1425408   106581          1000 stress-ng-vm
[ 2889.077588] Out of memory: Kill process 1588 (stress-ng-vm) score 1580 or sacrifice child
[ 2889.077806] Killed process 1588 (stress-ng-vm) total-vm:1589800kB, anon-rss:652676kB, file-rss:0kB, shmem-rss:0kB
[ 2889.274607] oom_reaper: reaped process 1588 (stress-ng-vm), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB

```
